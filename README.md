# README #

### What is this repository for? ###

* The first [Softuniada](https://softuniada.softuni.bg/softuniada-2016/) edition, December 2015 - January 2016
* Version 1.0.0

### How do I get set up? ###

* Clone the repository to a local folder
* Start localhost service (WAMP) or IIS (point Frontend folder as Physical Path) and run the backend solution
* Dependencies - Visual Studio 2015, Microsoft SQL Server 2014 Express
* Database configuration - right click on "DataPopulation.publish.xml" in back-end project "DataPopulation" and then click "Publish"

### Who do I talk to? ###

Ivan Zdravkov - ivanzdravkovbg@gmail.com
Marin Takanov - mbtakanov@yahoo.com